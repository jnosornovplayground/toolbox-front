import React, { useState } from 'react'
import ReactDOM from 'react-dom'

import Nav from './components/Nav'
import Logger from './components/Logger'

import "bootstrap";
import "bootstrap/dist/css/bootstrap.css" 
import 'regenerator-runtime/runtime'
import './style.scss'

import fetchEchoResponse from './client'

const App = () => {
  const [logs, setLogs] = useState([])
  
  const sendHanlder = async (text) => {
    const response = await fetchEchoResponse(text)

    const updatedLogs = [{ text, response: response.text }, ...logs]
    setLogs(updatedLogs)
  }

  return (
    <div>
     <Nav sendHandler={sendHanlder}/>
     <Logger logs={logs}/>
    </div>
  )
}

ReactDOM.render(<App/>, document.getElementById("root"))
