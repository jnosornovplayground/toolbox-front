async function fetchEchoResponse(text) {
  const API_URL = "http://localhost:3000"
  const response = await fetch(`${API_URL}/iecho?text=${text}`, {
    method: 'get',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
  })

  return response.json()
}

module.exports = fetchEchoResponse