import React, { useState } from 'react'
import PropTypes from 'prop-types'

const Nav = ({ sendHandler }) => {
  const [text, setText] = useState("")
  const [isSendDisabled, setSendDisabled] = useState(false)

  const onInputChange = (event) => {
    const value = event.target.value
    setText(value)
  }

  const onSubmit = async () => {
    setSendDisabled(true)
    await sendHandler(text)
    setText("")
    setSendDisabled(false)
  }

  return (
    <div className="navbar navbar-toolbox py-3">
    <div className="container px-0">
      <form className="form-inline w-100 row mx-0">
        <div className="col-sm-10 px-0">
          <input
            type="text"
            className="form-control mr-sm-2 w-100"
            placeholder="Insert text"
            aria-label="input"
            value={text}
            onChange={onInputChange}
            />
        </div>
        <div className="col-sm-2 px-0 pl-sm-4">
          <button
            disabled={isSendDisabled || !text.length}
            type="submit"
            className="btn btn-dark mt-2 mt-sm-0 w-100 font-weight-bold"
            onClick={onSubmit}
          >
            Send
          </button>
        </div>
      </form>
    </div>
  </div>
  )
}

Nav.propTypes = {
  sendHandler: PropTypes.func.isRequired
}

export default Nav