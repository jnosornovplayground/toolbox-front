import React from 'react'
import PropTypes from 'prop-types'

import Log from './Log'

const Logger = ({ logs }) => {
  const renderLogs = () => {
    return (
      <>
        {logs.map((log, index) => {
          return (
            <li key={index} className="mb-3">
              <Log text={log.text} response={log.response} />
            </li>
            )
        })}
      </>
    )
  }

  return (
    <div className="container-full">
      <div className="container px-0">
        <div className="logger bg-white mt-4 p-3 rounded shadow-sm">
          <h1 className="title">Echo logger</h1>
          <ul className="list-unstyled m-3">
            {logs.length ? renderLogs() : <h2 style={{ fontSize: "15px" }}>No logs yet</h2>}
          </ul>
        </div>
      </div>
     </div>
  )
}

Logger.propTypes = {
  logs: PropTypes.array.isRequired
}

export default Logger