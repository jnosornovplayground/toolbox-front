import React from 'react'
import PropTypes from 'prop-types'

const Log = ({ text, response }) => {
  return (
    <div className="log">
      <LogItem label="text" value={text}/>
      <LogItem label="response" value={response}/>
    </div>
  )
}

Log.propTypes = {
  text: PropTypes.string.isRequired,
  response: PropTypes.string.isRequired
}

export default Log

const LogItem = ({ label, value }) => {
  return (
    <div style={{ minWidth: "120px" }} className="d-inline-block mr-3">
      <span className="label mr-2 font-weight-bold">{label}</span>
      <p className="value m-0">{value}</p>
  </div>
  )
}

LogItem.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired
}